// Function to print console messages to HTML
function printToConsole(message, type) {
  var consoleDiv = document.getElementById("console");
  var messageClass = type === "error" ? "error" : type === "warn" ? "warn" : "log";
  consoleDiv.innerHTML += "<div class='" + messageClass + "'>" + message + "</div>";
}

// Replace console.log, console.error, console.warn with printToConsole
console.log = function(message) {
  printToConsole(message, "log");
};

console.error = function(message) {
  printToConsole(message, "error");
};

console.warn = function(message) {
  printToConsole(message, "warn");
};

// Function to capture console messages from iframe
window.addEventListener("message", function(event) {
  if (event.origin === "null" && event.data && event.data.type === "console") {
    var messageType = event.data.messageType;
    var consoleMessage = event.data.message;
    printToConsole(consoleMessage, messageType);
  }
});

// Function to open a specific tab
function openTab(tabName) {
  var tabs = document.getElementsByClassName("tab");
  for (var i = 0; i < tabs.length; i++) {
    tabs[i].style.display = "none";
  }
  document.getElementById(tabName).style.display = "block";
}

// Function to update iframe content
function updateResult() {
  var htmlCode = document.getElementById("htmlCode").value + "<div id='console-iframe-yn3v9y3'></div>";
  var cssCode = "<style>" + document.getElementById("cssCode").value + "</style>";
  var jsCode = "<script>window.parent.postMessage({ type: 'console', messageType: 'log', message: '' }, '*');try{" + document.getElementById("jsCode").value + "}catch(error){window.parent.postMessage({ type: 'console', messageType: 'error', message: error.toString() }, '*');}function logToConsole(message) {var consoleDiv = document.getElementById('console-iframe-yn3v9y3'));consoleDiv.innerHTML += message + '<br>';}console.log = function(message) {logToConsole(message);};window.addEventListener('error', function(event) {var errorMessage = event.message + ' at ' + event.filename + ':' + event.lineno;logToConsole(errorMessage);});</script>";
  var resultFrame = document.getElementById("resultFrame");
  var doc = resultFrame.contentWindow.document;
  doc.open();
  doc.write(htmlCode + cssCode + jsCode);
  doc.close();
}

// Event listeners for textarea changes
document.getElementById("htmlCode").addEventListener("input", updateResult);
document.getElementById("cssCode").addEventListener("input", updateResult);
document.getElementById("jsCode").addEventListener("input", updateResult);
