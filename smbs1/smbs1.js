var user = prompt("ユーザー名", "yoshikunTA");
async function getJson(projectid) {
  try {
    const tokenResponse = await fetch("https://api.scratch.mit.edu/projects/" + projectid);
    if (!tokenResponse.ok) {
      throw new Error(`Error fetching project token: ${tokenResponse.statusText}`);
    }
    const tokenData = await tokenResponse.json();
    const token = tokenData.project_token;

    const jsonResponse = await fetch("https://projects.scratch.mit.edu/" + projectid + "/?token=" + token);
    if (!jsonResponse.ok) {
      throw new Error(`Error fetching project JSON: ${jsonResponse.statusText}`);
    }
    const jsondata = await jsonResponse.json();
    return jsondata;
  } catch (error) {
    console.error(`Failed to get JSON for project ${projectid}:`, error);
    return null;
  }
}

async function checkBlocks(projectid) {
  const jsonData = await getJson(projectid);
  if (jsonData === null) {
    return;
  }
  const targets = jsonData.targets;
  if (targets === undefined) {
    return;
  } else {
    for (var i = 0; i < targets.length; i++) {
      const blocks = Object.values(targets[i].blocks);
      for (var j = 0; j < blocks.length; j++) {
        const opcode = blocks[j].opcode;
        if (opcodes[opcode] === undefined) {
          opcodes[opcode] = 1;
        } else {
          opcodes[opcode] += 1;
        }
      }
    }
  }
}

var opcodes = {};

async function getIdList() {
  var idList = [];
  var page = 0;
  while (true) {
    const response = await fetch("https://api.scratch.mit.edu/users/"+user+"/projects/?limit=40&offset=" + (page * 40));
    if (!response.ok) {
      throw new Error(`Error fetching projects: ${response.statusText}`);
    }
    const projects = await response.json();
    if (projects.length === 0) {
      break;
    }
    for (var i = 0; i < projects.length; i++) {
      idList.push(projects[i].id);
    }
    page++;
  }
  return idList;
}

(async () => {
  try {
    const idList = await getIdList();
    for (var i = 0; i < idList.length; i++) {
      await checkBlocks(idList[i]);
    }
    document.write(JSON.stringify(opcodes));
  } catch (error) {
    console.error("Error processing projects:", error);
  }
})();
