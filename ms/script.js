// script.js
const boardElement = document.getElementById('board');
let boardSize = 10;
let mineCount = 10;
let board = [];
let revealed = [];
let flags = [];
let gameOver = false;

function initializeBoard() {
    board = [];
    revealed = [];
    flags = [];
    gameOver = false;
    for (let i = 0; i < boardSize; i++) {
        board.push([]);
        revealed.push([]);
        flags.push([]);
        for (let j = 0; j < boardSize; j++) {
            board[i].push(0);
            revealed[i].push(false);
            flags[i].push(false);
        }
    }
    placeMines();
    calculateNumbers();
}

function placeMines() {
    let minesPlaced = 0;
    while (minesPlaced < mineCount) {
        const x = Math.floor(Math.random() * boardSize);
        const y = Math.floor(Math.random() * boardSize);
        if (board[x][y] === 0) {
            board[x][y] = -1;
            minesPlaced++;
        }
    }
}

function calculateNumbers() {
    for (let i = 0; i < boardSize; i++) {
        for (let j = 0; j < boardSize; j++) {
            if (board[i][j] === -1) {
                continue;
            }
            let count = 0;
            for (let dx = -1; dx <= 1; dx++) {
                for (let dy = -1; dy <= 1; dy++) {
                    const nx = i + dx;
                    const ny = j + dy;
                    if (nx >= 0 && nx < boardSize && ny >= 0 && ny < boardSize && board[nx][ny] === -1) {
                        count++;
                    }
                }
            }
            board[i][j] = count;
        }
    }
}

function revealEmpty(x, y) {
    if (x < 0 || x >= boardSize || y < 0 || y >= boardSize || revealed[x][y]) {
        return;
    }
    revealed[x][y] = true;
    if (board[x][y] === 0) {
        for (let dx = -1; dx <= 1; dx++) {
            for (let dy = -1; dy <= 1; dy++) {
                revealEmpty(x + dx, y + dy);
            }
        }
    }
}

function reveal(x, y) {
    if (gameOver || revealed[x][y] || flags[x][y]) {
        return;
    }
    revealed[x][y] = true;
    if (board[x][y] === 0) {
        revealEmpty(x, y);
    }
    if (board[x][y] === -1) {
        gameOver = true;
        alert('Game Over');
        revealMines();
    }
    checkWin();
}

function flag(x, y) {
    if (gameOver) {
        return;
    }
    flags[x][y] = !flags[x][y];
    checkWin();
}

function revealMines() {
    for (let i = 0; i < boardSize; i++) {
        for (let j = 0; j < boardSize; j++) {
            if (board[i][j] === -1) {
                revealed[i][j] = true;
            }
        }
    }
}

function checkWin() {
    let revealedCount = 0;
    for (let i = 0; i < boardSize; i++) {
        for (let j = 0; j < boardSize; j++) {
            if (revealed[i][j]) {
                revealedCount++;
            }
        }
    }
    if (revealedCount === boardSize * boardSize - mineCount) {
        gameOver = true;
        alert('You Win!');
    }
}

function renderBoard() {
    boardElement.innerHTML = '';
    for (let i = 0; i < boardSize; i++) {
        for (let j = 0; j < boardSize; j++) {
            const cell = document.createElement('div');
            cell.classList.add('cell');
            if (revealed[i][j]) {
                cell.textContent = board[i][j] === 0 ? '' : board[i][j];
            }
            if (flags[i][j]) {
                cell.classList.add('flag');
            }
            cell.addEventListener('click', () => reveal(i, j));
            cell.addEventListener('contextmenu', (e) => {
                e.preventDefault();
                flag(i, j);
            });
            boardElement.appendChild(cell);
        }
        boardElement.appendChild(document.createElement('br'));
    }
}

function changeDifficulty(difficulty) {
    switch (difficulty) {
        case 'easy':
            boardSize = 8;
            mineCount = 10;
            break;
        case 'medium':
            boardSize = 10;
            mineCount = 20;
            break;
        case 'hard':
            boardSize = 12;
            mineCount = 30;
            break;
    }
    resetBoard();
}

function resetBoard() {
    initializeBoard();
    renderBoard();
}

initializeBoard();
renderBoard();
