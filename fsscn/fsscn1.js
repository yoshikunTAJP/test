var user = prompt("スタジオID", "35486771");
var opcodes = 0;

async function getIdList() {
  var idList = [];
  var page = 0;
  while (true) {
    const response = await fetch("https://api.scratch.mit.edu/studios/"+user+"/comments/?offset=" + (page * 20));
    if (!response.ok) {
      throw new Error(`Error fetching projects: ${response.statusText}`);
    }
    console.log(response.json());
    if (response.json().length==0) {
      break;
    }
    opcodes += response.json().length;
    page++;
  }
  return idList;
}

(async () => {
  try {
    const idList = await getIdList();
    console.log(opcodes);
  } catch (error) {
    console.error("Error processing projects:", error);
  }
})();
